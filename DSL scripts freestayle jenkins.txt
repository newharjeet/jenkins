job('DSL-Tutorial-1-Test') {
    scm {
        git('git://github.com/quidryan/aws-sdk-test.git')
    }
    triggers {
        scm('H/15 * * * *')
    }
    steps {
        maven('-e clean test')
    }
}


job("DSLJob02") {
  description("Job created through DSL")
  scm {
    git {
      remote {
        url("https://github.com/saurabh0010/houselannister.git")
      }
      // branch("*/master")
    }
  }
}



job("DSLJob03") {
  description("Job created through DSL")
  scm {
    git{
      remote {
        url("https://github.com/iamAnupamSY/mavenJavaProject.git")
      }
      // branch("*/master")
    }
  }
  steps {
    maven { 
      goals('clean')
      goals('compile')
      mavenInstallation('apache-maven-3.5.2')
    }
  }
}

















job("DSLJob02") {
  description("Job created through DSL")
  scm {
    git {
      remote {
        url("https://github.com/saurabh0010/houselannister.git")
      }
      // branch("*/master")
    }
  }
  steps {
     maven { 
      goals('clean')
      goals('compile')
     }
  }
}



job("DSLJob05") {
  description("Job created through DSL")
  scm {
    git {
      remote {
        url("https://github.com/saurabh0010/houselannister.git")
      }
      // branch("*/master")
    }
  }
  triggers {
          cron('* * * * *')
           }
     steps {
        maven { 
          mavenInstallation('maven')
          goals('clean')
          goals('compile')
     }
  }
}

cobertura:cobertura


job("DSLJobCobertura") {
  description("Job created through DSL")
  scm {
    git {
      remote {
        url("https://github.com/saurabh0010/houselannister.git")
      }
      // branch("*/master")
    }
  }
  triggers {
          cron('* * * * *')
           }
     steps {
        maven { 
          mavenInstallation('maven')
          goals('clean')
          goals('compile')
          goals ('cobertura:cobertura')
     }
  }
}





job("DSLJobCobertura") {
  description("Job created through DSL")
  scm {
    git {
      remote {
        url("https://github.com/saurabh0010/houselannister.git")
      }
      // branch("*/master")
    }
  }
  triggers {
          cron('* * * * *')
           }
     steps {
        maven { 
          mavenInstallation('maven')
          goals('clean')
          goals('compile')
          goals (' findbugs:findbugs')
          goals ('checkstyle:checkstyle')
     }
  }
     publishers {
          checkstyle('**/target/checkstyle-result.xml') 
          }

}









publishers {
checkstyle('**/target/checkstyle-result.xml') 